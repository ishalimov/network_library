package com.example.ivan.aynyrvokrugnetwork.transport

class InputInterfaceImplementation():Thread(),InputInterface {

    var inputQueue:Queue? = null
    var isRun = false

    override fun run() {
        super.run()

        while(isRun){
            try {
                val packet = inputQueue?.pull()
            } catch (exception: Queue.QueueEmpty){
                // Если очередь пуста
            }

        }
    }

    override fun startWork() {
        this.start()
    }

    override fun setQueue(queue: Queue):InputInterface {

        return this
    }
}