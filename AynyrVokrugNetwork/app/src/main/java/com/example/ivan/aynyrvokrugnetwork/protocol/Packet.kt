package com.example.ivan.aynyrvokrugnetwork.protocol

interface Packet {

    fun getBytes():ByteArray
}