package com.example.ivan.aynyrvokrugnetwork.transport

import com.example.ivan.aynyrvokrugnetwork.protocol.Packet
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.BlockingQueue

class InputQueue:Queue {
    val queue:BlockingQueue<Packet> =  ArrayBlockingQueue<Packet>(1,true)

    override fun put(packet:Packet) {
    }

    override fun pull():Packet {
        throw Queue.QueueEmpty()
    }

    override fun clear() {

    }

    override fun getAll(): List<Packet> {
        return listOf()
    }

    class QueueOverflowException:Exception()

}