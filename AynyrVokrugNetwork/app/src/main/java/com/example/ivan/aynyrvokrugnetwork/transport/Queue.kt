package com.example.ivan.aynyrvokrugnetwork.transport

import com.example.ivan.aynyrvokrugnetwork.protocol.Packet

interface Queue {

    fun put(packet:Packet)
    fun pull():Packet
    fun clear()
    fun getAll():List<Packet>


    class QueueEmpty: Exception()
}