package com.example.ivan.aynyrvokrugnetwork.transport

interface InputInterface {

    fun startWork()
    fun setQueue(queue: Queue):InputInterface
}