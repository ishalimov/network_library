package com.example.ivan.aynyrvokrugnetwork

import com.example.ivan.aynyrvokrugnetwork.transport.InputInterface
import com.example.ivan.aynyrvokrugnetwork.transport.InputInterfaceImplementation
import com.example.ivan.aynyrvokrugnetwork.transport.InputQueue
import com.example.ivan.aynyrvokrugnetwork.transport.Queue

class SocketConnection(val port:Int,val serverAddres:String,val inputMechanism:InputInterface?) {
    private val inputQueue:Queue

    init {
        inputQueue = InputQueue()
    }

    fun connection(){
       inputMechanism?.setQueue(inputQueue)?.startWork()
    }

    class Builder() {
        var port: Int = -1
        var serverAddress:String = ""

        fun build() = SocketConnection(port,serverAddress, InputInterfaceImplementation())
    }
}